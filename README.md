Bootstrap 4 UI SASS Developer

This project give a quick easy way to see all Bootstrap 4 components in one place. Developers can see the global effects of BS variable overrides -- useful for theme development.  

Use sass/custom/custom-variables.scss to override the default Bootstrap _variables.

Use sass/custom/style.scss to override existing BS SASS, create new SASS rules, or import new SASS partials. This file compiles to /build/css/style.css

